# TurtleBot

[TurtleBot 2](http://www.turtlebot.com/) is a platform designed for research and education.
The main part is the Kobuki base, which provide basic sensors (bumper, wheel drop, etc), digital and analog inputs, digital outputs and actuators.
In addition to the Kobuki sensors, the TurtleBot 2 has a Kinect-like RGBD sensor.

 - Link: [IEEE Spectrum: TurtleBot 2](http://spectrum.ieee.org/automaton/robotics/diy/turtlebot-2-now-available-for-preorder-from-clearpath-robotics)
 - Video: [Introducing Yujin Robot's Kobuki](https://www.youtube.com/watch?v=t-KTHkbUwrU)

We have two types of TurtleBots with RGBD sensors:
 - turtle01 - turtle02: old TurtleBot 2 with Intel RealSense D435i,
 - turtle03 - turtle07: old TurtleBot 2 with Intel RealSense R200,
 - **turtle08 - turtle13 (recommended)**: new TurtleBot 2 with Intel RealSense D435.

There used to be Orbex Astra cameras on some robots. They had better resolution and
image quality than the RealSense cameras, but they did not provide depth. Since 2024,
no robots carry these cameras.

For RealSense cameras, the minimum range which can be measured may increase with resolution.

Issues with the TurtleBots
(hardware, sensor calibration, Singularity containers, ...)
can be reported at the
[RoboLab repository](https://gitlab.fel.cvut.cz/robolab/robolab/issues).

## Working with the TurtleBot

- Power up robot and onboard PC (NUC).
- **Note that switching off the robot will also cut power to the NUC.**
- After the boot, you should be able to log in
  - Using your username `ssh <username>@turtleXX` and your [lab password](https://cw.felk.cvut.cz/password/). 
  - Or, using guest account `ssh guest@turtleXX` with password `xxx`.
- If you like to open graphical windows on your desktop use `ssh -XYC ...`.
- Start singularity instance and enter the container (see [Using Singularity Containers](#using-singularity-containers)).
- At this point it is wise to learn and start using terminal multiplexer such
  as [tmux](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/).
- Assuming that you have followed the [Catkin Workspace Configuration](#catkin-workspace-configuration) instructions, everything should be ready.
- The robots are only visible from the local network, so in order to use
  personal notebooks use cable (preferred) or wifi (ssid: `turtlebots`, pass: `TurtlesDC`).
- After your work is finished, turn off the robot and the laboratory computer
  by typing `poweroff` in terminal. It will take about 10 seconds.
  PLEASE DO NOT type `shutdown`.


## Launching the TurtleBots

Tested with new TurtleBots (turtle08 - turtle13) and ROS Noetic (see below).

To launch robot drivers:
```bash
HOSTNAME=$(hostname) roslaunch robolab_bringup turtlebot2.launch
``` 

To launch keyboard controller:
```bash
roslaunch kobuki_keyop safe_keyop.launch
```

## Connecting to Remote ROS Master

- Set the `ROS_MASTER_URI` environmental variable to connect to a remote ROS master.
  For example, visualize robot topics from a desktop computer:
  ```
  ROS_MASTER_URI=http://turtle08:11311 rviz
  ```
- For nodes running on different computers to communicate, they must be able to resolve advertised names, which may not work on your computer.
  `ROS_IP` tells the node to advertise topics using the IP address (which the robot can see) instead of the hostname (which it may not see).
  In the local robot network, you can use the following:
  ```
  export ROS_IP=$(ip -4 addr | grep -oP '(?<=inet\s)192\.168\.65\.\d+')
  ```
- The IP address of turtleXX is 192.168.65.(20 + XX), that is 192.168.65.21 for turtle01 up to 192.168.65.33 for turtle13.
  On your computer, you can add these items to `/etc/hosts` to ensure the name resolution works in all cases:
  ```
  192.168.65.21  turtle01
  192.168.65.22  turtle02
  192.168.65.23  turtle03
  192.168.65.24  turtle04
  192.168.65.25  turtle05
  192.168.65.26  turtle06
  192.168.65.27  turtle07
  192.168.65.28  turtle08
  192.168.65.29  turtle09
  192.168.65.30  turtle10
  192.168.65.31  turtle11
  192.168.65.32  turtle12
  192.168.65.33  turtle13
  ```
   - However, name resolution should work out of the box. If you're connected via cable and it doesn't work, disable your wifi.


# Using Singularity Containers

On computers and robots in the labs, [ROS](https://ros.org/) is available through [Singularity containers](https://sylabs.io/guides/3.5/user-guide/introduction.html).

Singularity can be easily installed on Ubuntu using the PPA https://launchpad.net/~peci1/+archive/ubuntu/singularity-ce-v4 .

Note that individual courses may have their own recommendation on what image
to use and what workspace to source or extend.

More info about Singularity can be found at
[DCE Wiki](https://support.dce.felk.cvut.cz/mediawiki/index.php/singularity) (in Czech).


## ROS Noetic

- Start Ubuntu as Singularity container (`noetic`) and log into its shell
  ```bash
  singularity shell /local/robolab_noetic.simg
  ```
- Source default ROS workspace if you don't configure yours.
  ```bash
  source /opt/ros/robolab/setup.bash
  ```

## Singularity Instance

Singularity instances may be used instead.
```bash
singularity instance start /local/robolab_noetic.simg ros
singularity shell instance://ros
```

# Catkin Workspace Configuration

[Catkin tools](https://catkin-tools.readthedocs.io/en/latest/) provides command line tools for working with the Catkin meta-buildsystem and Catkin workspaces.
(See a [short summary](https://answers.ros.org/question/320613/catkin_make-vs-catkin_make_isolated-which-is-preferred/) of alternatives.) 

Note that individual courses may have their own recommendation on what
workspace to source or extend.

- Catkin tools allow you to extend some parent workspace (workspace of your course) and possibly overlay some packages the parent workspace contains.
- As the network home directories are used, you can setup your workspace on
  any robot or lab workstation.
  Lab workstation should be the preferred options due to its wired connection
  to the network filesystem and much lower latency.
  `/tmp` should be considered if even lower latency is needed.
- Be sure that you are inside the Singularity container (see above).

## ROS Noetic + Catkin Tools

```bash
ws=~/workspace/my_course
mkdir -p "${ws}/src"
cd "${ws}/src"
# Get and modify course-specific packages here.

cd "${ws}"
catkin init
catkin config --extend /opt/ros/robolab
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release
catkin build -c

source "${ws}/devel/setup.bash"
```

